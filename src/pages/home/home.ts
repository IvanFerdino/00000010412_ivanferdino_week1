import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SecondPage } from '../second/second';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }
  click_function() {
    console.log('Button is Clicked');
  }
  first_page_function(){
    console.log('page 1 is Clicked');
    this.navCtrl.push(SecondPage);
  }

}
